public class Personnage
{
    private String nom;
    private int tailleBarbe;
    private int tailleOreilles;
    
    public Personnage(String nom, int barbe, int oreilles){
        this.nom = nom;
        this.tailleBarbe = barbe;
        this.tailleOreilles = oreilles;
    }
    
    @Override
    public String toString(){
        return "("+this.nom+", b="+this.tailleBarbe+", o="+this.tailleOreilles+")";
    }
    
    
    // Autres méthodes pas nécessaires ici mais qui pourraient être utiles
    public String getNom(){ 
    return this.nom;
    }
    public int getBarbe(){
        return this.tailleBarbe;
    }
    public int getOreilles(){
        return this.tailleOreilles;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Personnage that = (Personnage) o;
        return nom.equals(that.nom) && tailleBarbe == that.tailleBarbe && tailleOreilles == that.tailleOreilles;
    }
    
    
    

}
